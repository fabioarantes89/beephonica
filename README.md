# BeePhonica

 - O cliente poderá calcular o valor da ligacão com plano e sem plano.
 - Ali o cliente pode escolher:
    - Códigos de cidade de Destino e Origem
    - Tempo em minutos
    - Plano
# BeePhonica

Projeto executado para demonstrar um pouco do conhecimento.

Não foi utilizado bootstrap ou similar a fim de que vocês possam verificar que eu realmente conheço um pouco de CSS.

## Instalação

Para rodar este projeto é necessário ter o Yarn. Para isto é só rodar:
```
npm install -g yarn
```

Feito isto, entrar na pasta do projeto e instalar as dependencias:

```
yarn install
```

Para rodar a aplicação em modo de desenvolvimento, é só executar:
```
yarn start
```

### Build

Para fazer build da aplicação é só executar:

```
yarn build
```


### Docker

Foi adicionado também um dockerfile com uma imagem básica do NGINX 

Para usá-la

Crie a image:

```
docker build -t beephonica/calculadora .
```

Executar:

```
docker run -p 8080:80 --name beephonica -d beephonica/calculadora
```

