
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore } from "redux";
import rootReducer from './reducers';
import logo from './images/Beephonica.png';
import './App.css';
import CallList from './containers/callList';
import CallDialog from './containers/callDialog';
import Mocks from './mocks';


let store = createStore(rootReducer, Mocks);
class App extends Component {
  render() {
    return <Provider store={store}>
        <div className="App">
          <header className="header">
            <img src={logo} alt="logo" className="logo" />
            <h1 className="title">
              <strong>Bee</strong>Phonica
            </h1>
          </header>
          <h1>Bem vindo a BeePhonica</h1>
          <p>
            Aqui você pode calcular quanto você gasta aproximadamente com
            ligações interurbanas.
          </p>
          <p>Para começar selecione os planos que você deseja comparar:</p>
          <div className="one-pane">
            <div className="mainContent">
              <CallList />
              <CallDialog />
            </div>
          </div>
        </div>
      </Provider>;
  }
}

export default App;
