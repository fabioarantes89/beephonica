const BeePhoneCallMaps = (state = [], action) => {
  switch (action) {
    case "ADD_CALLMAP":
      return [
        ...state,
        {
          source: action.source,
          destiny: action.destiny,
          price: action.price
        }
      ];
    default:
      return state;
  }
};
export default BeePhoneCallMaps;
