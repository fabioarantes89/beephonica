const DDDs = (state = [], action) => {
  switch (action) {
    case "ADD_DDD":
      return [
        ...state,
        {
          code: action.code,
          name: action.name,
          state: action.state
        }
      ];
    case "REMOVE_DDD":
      return state.filter(item => item.code !== action.code);
    default:
      return state;
  }
};
export default DDDs;
