const BeePhoneCalls = (state = [], action) => {
    switch (action.type) {
      case "ADD_CALL":
      
        return [...state, { 
            callMap: action.callMap,
            seconds: action.seconds,
            plan: action.plan
        }];
      case "REMOVE_CALL":
        
        return state.filter((item) => !Object.is(item, action.call));
      default:
        return state;
    }
}
export default BeePhoneCalls;