const BeePhonePlans = (state = [], action) => {
  switch (action) {
    case "ADD_PLAN":
      return [
        ...state,
        {
          id: action.id,
          minutes: action.minutes,
          tax: action.tax,
          excedingTax: action.excedingTax
        }
      ];
    case "REMOVE_PLAN":
      return state.filter(item => item.id !== action.id);
    default:
      return state;
  }
};
export default BeePhonePlans;
