import { combineReducers } from 'redux';
import BeePhoneCalls from './BeePhoneCalls';
import BeePhonePlans from "./BeePhonePlans";
import BeePhoneCallMaps from "./CallMaps";
import BeeDialog from "./BeeDialog";
import DDDs from "./DDDs";

export default combineReducers({
  calls: BeePhoneCalls,
  plans: BeePhonePlans,
  ddds: DDDs,
  callMaps: BeePhoneCallMaps,
  activeDialog: BeeDialog
});
