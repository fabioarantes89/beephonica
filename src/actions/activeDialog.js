export const openDialog = () => ({
  type: "open"
});
export const closeDialog = () => ({
  type: "close"
});

export const toggleDialog = () => ({
  type: "toggle"
});

export default {
  openDialog,
  closeDialog,
  toggleDialog
};
