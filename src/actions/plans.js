export const addPlan = (minutes, tax, exceding) => ({
  type: "ADD_PLAN",
  id: `BeePhonica ${minutes}`,
  minutes,
  tax,
  exceding
});
export const removePlan = (minutes) => ({
  type: "REMOVE_PLAN",
  id: `BeePhonica ${minutes}`
});
export const addActivePlan = (plan) => ({
  type: "ADD_ACTIVE_PLAN",
  plan
});
export const removeActivePlan = (plan) => ({
  type: "REMOVE_ACTIVE_PLAN",
  plan
});
export const removeAllActivePlans = () => ({ type: "REMOVE_ALL_ACTIVE_PLANS" });

export default {
  addPlan,
  removePlan,
  addActivePlan,
  removeActivePlan,
  removeAllActivePlans
};
