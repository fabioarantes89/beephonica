export const addCall = (callMap, seconds, plan) => ({
  type: "ADD_CALL",
  callMap,
  seconds,
  plan
});
export const removeCall = (call) => ({
  type: "REMOVE_CALL",
  call
});

export const addCallMap = (source, destiny, price) => ({
  type: "ADD_CALLMAP",
  source,
  destiny,
  price
});


export default {
  addCall,
  removeCall,
  addCallMap
};
