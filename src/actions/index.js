import Calls from './calls';
import Plans from "./plans";
import Dialog from './activeDialog';
export default {
  Calls,
  Plans,
  Dialog
};