import Plans from './plans';
import DDDs from './ddds';
let callMaps = [
    {
      source: DDDs[0],
      destiny: DDDs[1],
      price: 190
    },
    {
      source: DDDs[2],
      destiny: DDDs[3],
      price: 290
    },
    {
      source: DDDs[0],
      destiny: DDDs[3],
      price: 170
    }
  ];

export default {
  plans: Plans,
  ddds: DDDs,
  callMaps: callMaps,
  activeDialog: false,
  calls: [
    {
      callMap: callMaps[1],
      seconds: 3600,
      plan: Plans[0]
    },
    {
      callMap: callMaps[2],
      seconds: 20,
      plan: Plans[2]
    },
    {
      callMap: callMaps[0],
      seconds: 40,
      plan: Plans[4]
    }
  ]
};