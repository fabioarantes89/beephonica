export default [
  {
    code: 11,
    name: "São Paulo",
    state: "SP"
  },
  {
    code: 19,
    name: "Campinas e região",
    state: "SP"
  },
  {
    code: 21,
    name: "Rio de Janeiro",
    state: "RJ"
  },
  {
    code: 49,
    name: "Maringá",
    state: "PR"
  }
];