export default [
  {
    name: "BeePhonica 20",
    minutes: 20,
    exceed: 1.1
  },
  {
    name: "BeePhonica 40",
    minutes: 40,
    exceed: 1.1
  },
  {
    name: "BeePhonica 60",
    minutes: 60,
    exceed: 1.1
  },
  {
    name: "BeePhonica 80",
    minutes: 80,
    exceed: 1.1
  },
  {
    name: "BeePhonica 120",
    minutes: 120,
    exceed: 1.1
  }
];