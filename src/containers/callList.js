import { connect } from 'react-redux';
import Actions from '../actions';
import CallList from '../components/callList';
const { Calls, Dialog } = Actions;

const mapStateToProps = state => ({
    calls: state.calls,
    activePlans: state.activePlans
});
const mapActionsToProps = dispatch => ({
  addCall: (minutes, source, destiny) =>
    dispatch(Calls.addCall(minutes, source, destiny)),
  removeCall: (minutes, source, destiny) =>
    dispatch(Calls.removeCall(minutes, source, destiny)),
  openDialog: () => dispatch(Dialog.openDialog())
});

export default connect(mapStateToProps, mapActionsToProps)(CallList);