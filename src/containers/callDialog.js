import { connect } from "react-redux";
import Actions from "../actions";
import CallDialog from "../components/callDialog";
const { Calls, Dialog } = Actions;

const mapStateToProps = state => ({
  callsMaps: state.callMaps,
  DDDs: state.ddds,
  Plans: state.plans,
  activeDialog: state.activeDialog
});
const mapActionsToProps = dispatch => ({
  addCall: (minutes, source, destiny) => 
    dispatch(Calls.addCall(minutes, source, destiny)),
  removeCall: (minutes, source, destiny) =>
    dispatch(Calls.removeCall(minutes, source, destiny)),
  closeDialog: () => dispatch(Dialog.closeDialog())
});

export default connect(mapStateToProps, mapActionsToProps)(CallDialog);
