import React from "react";

class CallDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = { duration: "", source: "", destiny: "", dests: [], errors: {}};
    this.validData = this.validData.bind(this);
    this.sendData = this.sendData.bind(this);
    this.changeDuration = this.changeDuration.bind(this);
    this.handleDestiny = this.handleDestiny.bind(this);
    this.handleSource = this.handleSource.bind(this);
    this.handlePlan = this.handlePlan.bind(this);
    this.convertMinutes = this.convertMinutes.bind(this);
  }
  sendData() {
    let errors = this.validData();

    if(Object.keys(errors).length === 0){
      let callMap = this.props.callsMaps.filter(callMap => {
        return parseInt(this.state.destiny, 10) === callMap.destiny.code && parseInt(this.state.source, 10) === callMap.source.code;
      })[0];
      let seconds = this.convertMinutes(this.state.duration);
      let plan = this.props.Plans.filter(item => item.name === this.state.plan)[0];
      this.props.addCall(callMap, seconds, plan);
      this.setState({ duration: "", source: "", destiny: "", dests: [] });
      this.props.closeDialog();
    } else {
      this.setState({errors})
    }
      
  }
  validData() {
    let errors = {};
    if (!this.state.source) {
      errors.source = { message: "Campo origem não pode ficar em branco" };
    }
    if (!this.state.destiny) {
      errors.source = { message: "Campo destino não pode ficar em branco" };
    }
    if (!this.state.duration) {
      errors.duration = { message: "Campo duração não pode ficar em branco" };
    }
    if (!this.state.plan) {
      errors.plan = { message: "Campo plano não pode ficar em branco" };
    }
    if (!/([0-9]+):([0-9]{2})/i.exec(this.state.duration)) {
      errors.duration = {
        message: "Campo duração deve seguir o padrão de 00:00 (Minutos:Segundos)"
      };
    }
    return errors;
  }
  convertMinutes(value){
    let parseDate = /([0-9]+):([0-9]{2})/i.exec(value);
    let seconds = parseInt(parseDate[2], 10);
    let minutes = parseInt(parseDate[1], 10);
    return (seconds + (minutes * 60));
  }
  handleDestiny(event) {
    this.setState({ destiny: event.target.value, errors: {}});
  }
  handlePlan(event) {
    this.setState({ plan: event.target.value, errors: {} });
  }
  handleSource(event) {
    let callMaps = this.props.callsMaps.filter(
      item => item.source.code === parseInt(event.target.value, 10)
    );
    let destinations = callMaps.map(callMap => {
      return callMap.destiny;
    });
    this.setState({
      source: event.target.value,
      dests: destinations,
      errors: {}
    });
  }
  changeDuration(event) {
    this.setState({
      duration: event.target.value.replace(/[^0-9:]/g, ""),
      errors: {}
    });
  }
  render() {
    return <div className={this.props.activeDialog ? "open" : "closed"}>
        <div className="overlay" onClick={() => {
            this.props.closeDialog();
          }} />
        <div className="dialog box">
          <h2>
            Adicionar Chamada
            <button className="closeBtn" onClick={() => {
                this.props.closeDialog();
              }}>
              X
            </button>
          </h2>
          <div className="content">
            <form>
              { (Object.keys(this.state.errors).length >= 1) ? <ul className="errors">
                  {Object.keys(this.state.errors).map((item, index) => {
                    return <li key={index}>
                        {this.state.errors[item].message}
                      </li>;
                  })}
                </ul> : ""}

              <div>
                <label htmlFor="source">Origem:</label>
              </div>
              <div>
                <select name="source" value={this.state.source} onChange={this.handleSource}>
                  <option value="">Selecione uma origem</option>
                  {this.props.DDDs.map((ddd, index) => {
                    return <option key={index} value={ddd.code}>
                        {ddd.name}
                      </option>;
                  })}
                </select>
              </div>
              <div>
                <label htmlFor="destiny">Destino:</label>
              </div>
              <div>
                <select name="destiny" value={this.state.destiny} onChange={this.handleDestiny}>
                  <option value="">Selecione um destino</option>
                  {this.state.dests.map((ddd, index) => {
                    return <option key={index} value={ddd.code}>
                        {ddd.name}
                      </option>;
                  })}
                </select>
              </div>
              <div>
                <label htmlFor="destiny">Destino:</label>
              </div>
              <div>
                <select name="plano" value={this.state.plan} onChange={this.handlePlan}>
                  <option value="">Selecione um plano</option>
                  {this.props.Plans.map((item, index) => {
                    return <option key={index} value={item.name}>
                        {item.name}
                      </option>;
                  })}
                </select>
              </div>
              <div>
                <label htmlFor="duration">Duração:</label>
              </div>
              <div>
                <input type="text" value={this.state.duration} onChange={this.changeDuration} />
              </div>
              <button type="button" onClick={() => {
                  this.sendData();
                }}>
                Adicionar
              </button>
              <p className="infText">
                A chamada que deseja não está na lista? <a href="#sendMail">
                  clique aqui
                </a>
              </p>
            </form>
          </div>
        </div>
      </div>;
  }
}
export default CallDialog;
