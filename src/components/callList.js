import React from 'react';
import { formatTime, calculateTax, formatPrice, calculateNormalPrice } from "../libs";

class CallList extends React.Component {
    removeCall(call){
      this.props.removeCall(call);
    }
    openDialog() {
      this.props.openDialog();
    }
    render(){
        return <div className="box">
            <h2>Chamadas</h2>
            <form>
              <table className="callList">
                <thead>
                  <tr>
                    <th>Origem</th>
                    <th>Destino</th>
                    <th>Duração</th>
                    <th>Plano Fale Mais </th>
                    <th>Com Fala Mais</th>
                    <th>Sem Fala Mais</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>

                  {this.props.calls.map((callItem, index) => {
                    return <tr key={index}>
                        <td>
                          {callItem.callMap.source.name} ({callItem.callMap.source.code})
                        </td>
                        <td>
                          {callItem.callMap.destiny.name} ({callItem.callMap.destiny.code})
                        </td>
                        <td>{formatTime(callItem.seconds)}</td>
                        <td>{callItem.plan.name}</td>
                        <td>
                          {formatPrice(
                            calculateTax(
                              callItem.plan,
                              callItem.callMap.price,
                              callItem.seconds
                            )
                          )}
                        </td>
                        <td>
                          {formatPrice(
                            calculateNormalPrice(
                              callItem.callMap.price,
                              callItem.seconds
                            )
                          )}
                        </td>
                        <td>
                          <button type="button" className="removeBtn" onClick={() => {this.removeCall(callItem);}}>
                            Remover
                          </button>
                        </td>
                      </tr>;
                  })}
                </tbody>
              </table>
            </form>
            <button type="button" className="icn-plus" onClick={() => { this.openDialog();}}>
              Adicionar chamada
            </button>
          </div>;
    }
}
export default CallList;