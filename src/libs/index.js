export const formatNumber = number => {
  return number < 10 ? `0${number}` : number;
};
export const formatTime = seconds => {
  let minutes = Math.floor(seconds / 60);
  let segs = seconds % 60;
  return `${formatNumber(minutes)}:${formatNumber(segs)}`;
};
export const calculateTax = (plan, callTax, seconds) => {
  let planQuota = plan.minutes * 60;
  if(seconds < planQuota) {
    return 0;
  }
  let exceededMinutes = (seconds - planQuota) / 60;
  let price = (exceededMinutes * callTax) * plan.exceed;
  return price;
}
export const calculateNormalPrice = (callTax, seconds) => {
  let plan = {
    minutes: 0,
    exceed: 1
  };
  return calculateTax(plan, callTax, seconds);
}

export const formatPrice = (price) => {
  return String(`R$ ${(price / 100).toFixed(2)}`).replace(".", ",");
}


export default {
  formatNumber,
  formatTime,
  calculateTax,
  formatPrice,
  calculateNormalPrice
};